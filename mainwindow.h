#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "util.h"
#include "net.h"

#include <string>
#include <vector>
#include <arpa/inet.h>

#include <QDebug>

#include <QtWidgets>
#include <QPushButton>
#include <QGridLayout>
#include <QTabWidget>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QLabel>
#include <QCheckBox>
#include <QRadioButton>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow();
    ~MainWindow();

public slots:
    void changeProtocolWidget(int);
    void changeOSILayers(int);
    void echoReplySelect(void){displayICMPCode(0);}
    void dstUnreachSelect(void){displayICMPCode(3);}
    void srcExtinctSelect(void){displayICMPCode(4);}
    void redirectSelect(void){displayICMPCode(5);}
    void echoRequestSelect(void){displayICMPCode(8);}
    void sendDataEvent();
    void useMyIP(bool check)
    {
        sIP->setDisabled(check);
        if(check){
            std::string cmd = "ifconfig "+ interfaces->currentText().toStdString() + " | grep \"inet \"|tr -s ' '|cut -d' ' -f3 | cut -c 5-";
            std::string ip = Util::exec(cmd.c_str());
            sIP->setText(QString::fromStdString(ip));
        }
    }
    void useMyMAC(bool check)
        {
            srcMAC->setDisabled(check);
            if(check){
                std::string cmd = "ifconfig "+ interfaces->currentText().toStdString() + " | grep 'Link' | tr -s ' '|cut -d' ' -f5";
                std::string ip = Util::exec(cmd.c_str());
                srcMAC->setText(QString::fromStdString(ip));
            }
        }

private:
    QPushButton *send;

    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *forge;
    QGridLayout *gridLayout;
    QComboBox *osiLayers;
    QComboBox *interfaces;
    QLineEdit *dPort;
    QLineEdit *sPort;
    QComboBox *protocole;
    QLabel *label_3;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *sIP;
    QLineEdit *IP;
    QLabel *label_4;
    QLabel *label_5;
    QPlainTextEdit *payload;

    QSlider *pingSize;
    QLCDNumber *pingSizeIndicator;
    QCheckBox *myIP;
    QRadioButton *echoReply;//type 0
    QRadioButton *dstUnreach;// type 3
    QRadioButton *srcExtinction; //type 4
    QRadioButton *redirect; //type 5
    QRadioButton *echoRequest;//type 8
    QComboBox *code;

    QCheckBox *myMAC;
    QLineEdit *srcMAC;
    QLineEdit *dstMAC;
    QLabel *srcMAC_label;
    QLabel *dstMAC_label;
    QComboBox *opCode;

    QLabel *hdwType_label;
    QLabel *ptcType_label;
    QLabel *hdwSize_label;
    QLabel *ptcSize_label;
    QLabel *opCode_label;



    void setupComposant(void);
    void resetForge(int index);
    void resetInterface();

    void displayApplicationProtocol(int index);
    void displayTransportProtocol(int index);
    void displayNetworkProtocol(int index);
    void displayDataLinkProtocol(int index);

    void displayICMPCode(int icmpType);
};

#endif // MAINWINDOW_H
