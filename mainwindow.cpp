#include "mainwindow.h"
#include "ui_mainwindow.h"

int osiIndex = 0;
int ptcIndex = 0;


MainWindow::MainWindow() : QWidget()
{
    setupComposant();
}

MainWindow::~MainWindow()
{

}

void MainWindow::setupComposant(void)
{
            this->setWindowModality(Qt::NonModal);
            this->resize(850, 550);
            this->setLayoutDirection(Qt::LeftToRight);

            gridLayout_2 = new QGridLayout(this);
            tabWidget = new QTabWidget(this);
            tab = new QWidget();
            forge = new QWidget();

            tabWidget->addTab(tab, QString());
            tabWidget->addTab(forge, QString());
            tabWidget->setTabText(tabWidget->indexOf(tab), "tab1");
            tabWidget->setTabText(tabWidget->indexOf(forge), "Craft");
            tabWidget->setCurrentIndex(0);
            gridLayout_2->addWidget(tabWidget, 0, 0, 1, 1);

            gridLayout = new QGridLayout(forge);

            osiLayers = new QComboBox(forge);
            osiLayers->clear();
            osiLayers->insertItems(0, QStringList()
                << "1: Physical" << "2: Data Link" << "3: Network"
                << "4: Transport" << "5: Session" << "6: Presentation" << "7: Application"
            );
            gridLayout->addWidget(osiLayers, 0, 0, 1, 1);

            interfaces = new QComboBox(forge);
            interfaces->clear();
            resetInterface();
            gridLayout->addWidget(interfaces, 0, 1, 1, 1);

            protocole = new QComboBox(forge);
            protocole->setEditable(false);
            protocole->clear();
            protocole->insertItem(0, "PHYSICAL");
            protocole->setCurrentIndex(0);
            gridLayout->addWidget(protocole, 0, 2, 1, 1);

            send = new QPushButton(this);
            send->setText("Send");
            gridLayout_2->addWidget(send, 1, 0, 1, 1);

            QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));
            QObject::connect(osiLayers, SIGNAL(currentIndexChanged(int)), this, SLOT(changeOSILayers(int)));

            QMetaObject::connectSlotsByName(this);
}

void MainWindow::resetInterface()
{
    vector<std::string> interface = Util::getInterfaces();

    for(auto it = interface.begin(); it != interface.end(); ++it)
    {
        interfaces->addItem(QString().fromStdString(*it) );
    }
}

void MainWindow::resetForge(int index)
{
    gridLayout->removeWidget(forge);
    tabWidget->removeTab(1);

    forge = new QWidget();

    tabWidget->addTab(forge, QString());
    tabWidget->setTabText(tabWidget->indexOf(forge), "Craft");
    tabWidget->setCurrentIndex(1);

    gridLayout = new QGridLayout(forge);

    osiLayers = new QComboBox(forge);
    osiLayers->clear();
    osiLayers->insertItems(0, QStringList()
        << "1: Physical" << "2: Data Link" << "3: Network"
        << "4: Transport" << "5: Session" << "6: Presentation" << "7: Application"
    );
    osiLayers->setCurrentIndex(index);
    osiLayers->setFocus();
    gridLayout->addWidget(osiLayers, 0, 0, 1, 1);

    interfaces = new QComboBox(forge);
    interfaces->clear();
    resetInterface();
    gridLayout->addWidget(interfaces, 0, 1, 1, 1);


    send = new QPushButton(this);
    send->setText("Send");
    gridLayout_2->addWidget(send, 1, 0, 1, 1);
    QObject::connect(send, SIGNAL(clicked()), this, SLOT(sendDataEvent()));

    QObject::connect(osiLayers, SIGNAL(currentIndexChanged(int)), this, SLOT(changeOSILayers(int)));
    QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

}

void MainWindow:: changeOSILayers(int index)
{
    osiIndex = index;
    resetForge(index);
    switch(index)
    {
    case 0:
        break;
    case 1:
        protocole = new QComboBox(forge);
        protocole->clear();
        protocole->insertItems(0, QStringList()
           << "CDP" << "CAN"
        );
        gridLayout->addWidget(protocole, 0, 2, 1, 1);

        QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

        changeProtocolWidget(0);
        break;
    case 2:
        protocole = new QComboBox(forge);
        protocole->clear();
        protocole->insertItems(0, QStringList()
            << "OSPF" << "RIP" << "ICMP" << "ARP"
        );
        gridLayout->addWidget(protocole, 0, 2, 1, 1);

        QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

        changeProtocolWidget(0);

        break;
    case 3:
        protocole = new QComboBox(forge);
        protocole->clear();
        protocole->insertItems(0, QStringList()
                << "TCP" << "UDP"
        );
        gridLayout->addWidget(protocole, 0, 2, 1, 1);
        protocole->setCurrentIndex(index);
        QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));
        changeProtocolWidget(0);
        break;
    case 4:
        break;
    case 5:
        break;
    case 6:
        protocole = new QComboBox(forge);
        protocole->clear();
        protocole->insertItems(0, QStringList()
            << "DHCP" << "HTTP" << "DNS" << "BGP" << "SMTP" << "Telnet"
        );
        gridLayout->addWidget(protocole, 0, 2, 1, 1);

        QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));
        changeProtocolWidget(0);
        break;
    }
}

void MainWindow::changeProtocolWidget(int index)
{
    switch(osiIndex)
    {
    case 0:
        break;
    case 1:
        displayDataLinkProtocol(index);
        break;
    case 2:
        displayNetworkProtocol(index);
        break;
    case 3:
        displayTransportProtocol(index);
        break;
    case 4:
        break;
    case 5:
        break;
    case 6:
        displayApplicationProtocol(index);
        break;

    }
}

void MainWindow::displayDataLinkProtocol(int index)
{
    resetForge(osiIndex);

    protocole = new QComboBox(forge);
    protocole->clear();
    protocole->insertItems(0, QStringList()
            << "CDP" << "CAN"
    );
    gridLayout->addWidget(protocole, 0, 2, 1, 1);
    protocole->setCurrentIndex(index);

    QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

    ptcIndex = index;

}

void MainWindow::displayNetworkProtocol(int index)
{
    resetForge(osiIndex);

    protocole = new QComboBox(forge);
    protocole->clear();
    protocole->insertItems(0, QStringList()
            << "OSPF" << "RIP" << "ICMP" << "ARP"
    );
    gridLayout->addWidget(protocole, 0, 2, 1, 1);
    protocole->setCurrentIndex(index);

    QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));


    ptcIndex = index;

    switch(index)
        {
            case 0://OSPF
                break;
            case 1://RIP
                break;
            case 2://ICMP -s 65507

                label_5 = new QLabel(forge);
                label_5->setText("sIP:");
                gridLayout->addWidget(label_5, 0, 3, 1, 1);

                sIP = new QLineEdit(forge);
                gridLayout->addWidget(sIP, 0, 4, 1, 1);

                label = new QLabel(forge);
                label->setText("dIP:");
                gridLayout->addWidget(label, 0, 5, 1, 1);

                IP = new QLineEdit(forge);
                gridLayout->addWidget(IP, 0, 6, 1, 1);

                label_2 = new QLabel(forge);
                label_2->setText("size:");
                gridLayout->addWidget(label_2, 0, 7, 1, 1);

                pingSize = new QSlider(Qt::Horizontal, forge);
                pingSize->setMaximum(65507);
                gridLayout->addWidget(pingSize, 0, 8, 1, 1);

                pingSizeIndicator = new QLCDNumber(forge);
                gridLayout->addWidget(pingSizeIndicator, 0, 9, 1, 1);


                myIP = new QCheckBox(forge);
                myIP->setText("Use my IP");
                gridLayout->addWidget(myIP, 1, 4, 1, 1);

                echoRequest = new QRadioButton(forge);
                echoRequest->setText("Echo Request");
                gridLayout->addWidget(echoRequest, 1, 0, 1, 1);
                echoReply = new QRadioButton(forge);
                echoReply->setText("Echo Reply");
                gridLayout->addWidget(echoReply, 2, 0, 1, 1);
                dstUnreach = new QRadioButton(forge);
                dstUnreach->setText("Destination Unreachable");
                gridLayout->addWidget(dstUnreach, 3, 0, 1, 1);
                srcExtinction = new QRadioButton(forge);
                srcExtinction->setText("Source Extinction");
                gridLayout->addWidget(srcExtinction, 4, 0, 1, 1);
                redirect = new QRadioButton(forge);
                redirect->setText("Redirect");
                gridLayout->addWidget(redirect, 5, 0, 1, 1);

                code = new QComboBox(forge);
                code->setEditable(false);

                QObject::connect(echoReply, SIGNAL(clicked()), this, SLOT(echoReplySelect()));
                QObject::connect(dstUnreach, SIGNAL(clicked()), this, SLOT(dstUnreachSelect()));
                QObject::connect(srcExtinction, SIGNAL(clicked()), this, SLOT(srcExtinctSelect()));
                QObject::connect(redirect, SIGNAL(clicked()), this, SLOT(redirectSelect()));
                QObject::connect(echoRequest, SIGNAL(clicked()), this, SLOT(echoRequestSelect()));

                QObject::connect(myIP, SIGNAL(clicked(bool)), this, SLOT(useMyIP(bool)));
                QObject::connect(pingSize, SIGNAL(valueChanged(int)), pingSizeIndicator, SLOT(display(int)));

                break;
            case 3://ARP
                label_5 = new QLabel(forge);
                label_5->setText("sIP:");
                gridLayout->addWidget(label_5, 0, 3, 1, 1);

                sIP = new QLineEdit(forge);
                gridLayout->addWidget(sIP, 0, 4, 1, 1);

                label = new QLabel(forge);
                label->setText("dIP:");
                gridLayout->addWidget(label, 0, 5, 1, 1);

                IP = new QLineEdit(forge);
                gridLayout->addWidget(IP, 0, 6, 1, 1);

                srcMAC_label = new QLabel(forge);
                srcMAC_label->setText("sMAC:");
                gridLayout->addWidget(srcMAC_label, 1, 3, 1, 1);

                srcMAC = new QLineEdit(forge);
                gridLayout->addWidget(srcMAC, 1, 4, 1, 1);

                dstMAC_label = new QLabel(forge);
                dstMAC_label->setText("dMAC:");
                gridLayout->addWidget(dstMAC_label, 1, 5, 1, 1);

                dstMAC = new QLineEdit(forge);
                gridLayout->addWidget(dstMAC, 1, 6, 1, 1);

                myIP = new QCheckBox(forge);
                myIP->setText("Use my IP");
                gridLayout->addWidget(myIP, 2, 4, 1, 1);

                myMAC = new QCheckBox(forge);
                myMAC->setText("Use my MAC");
                gridLayout->addWidget(myMAC, 2, 6, 1, 1);

                opCode_label = new QLabel(forge);
                opCode_label->setText("Operation Code");
                gridLayout->addWidget(opCode_label, 1, 0, 1, 1);

                opCode = new QComboBox(forge);
                opCode->insertItems(0, QStringList() << "1. Request" << "2. Reply" );
                gridLayout->addWidget(opCode, 1, 1, 1, 1);


                QObject::connect(myIP, SIGNAL(clicked(bool)), this, SLOT(useMyIP(bool)));
                QObject::connect(myMAC, SIGNAL(clicked(bool)), this, SLOT(useMyMAC(bool)));

                break;
        }
}

void MainWindow::displayTransportProtocol(int index)
{
    resetForge(osiIndex);

    protocole = new QComboBox(forge);
    protocole->clear();
    protocole->insertItems(0, QStringList()
            << "TCP" << "UDP"
    );
    gridLayout->addWidget(protocole, 0, 2, 1, 1);
    protocole->setCurrentIndex(index);

    QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

    ptcIndex = index;

    switch(index){
    case 0:
        label_5 = new QLabel(forge);
        label_5->setText("sIP:");
        gridLayout->addWidget(label_5, 0, 3, 1, 1);

        sIP = new QLineEdit(forge);
        gridLayout->addWidget(sIP, 0, 4, 1, 1);

        label = new QLabel(forge);
        label->setText("dIP:");
        gridLayout->addWidget(label, 0, 5, 1, 1);

        IP = new QLineEdit(forge);
        gridLayout->addWidget(IP, 0, 6, 1, 1);

        label_2 = new QLabel(forge);
        label_2->setText("dPort:");
        gridLayout->addWidget(label_2, 0, 7, 1, 1);

        dPort = new QLineEdit(forge);
        dPort->setValidator( new QIntValidator(0, 65535, this) );
        gridLayout->addWidget(dPort, 0, 8, 1, 1);

        label_3 = new QLabel(forge);
        label_3->setText("sPort:");
        gridLayout->addWidget(label_3, 0, 9, 1, 1);

        sPort = new QLineEdit(forge);
        sPort->setValidator( new QIntValidator(0, 65535, this) );
        gridLayout->addWidget(sPort, 0, 10, 1, 1);

        label_4 = new QLabel(forge);
        label_4->setText("Payload:");
        gridLayout->addWidget(label_4, 1, 0, 1, 1);

        payload = new QPlainTextEdit(forge);
        gridLayout->addWidget(payload, 2, 0, 1, 8);

        break;
    case 1:
        break;
    }
}


void MainWindow::displayApplicationProtocol(int index)
{
    resetForge(osiIndex);

    protocole = new QComboBox(forge);
    protocole->clear();
    protocole->insertItems(0, QStringList()
            << "DHCP" << "HTTP" << "DNS" << "BGP" << "SMTP" << "Telnet"
    );
    gridLayout->addWidget(protocole, 0, 2, 1, 1);
    protocole->setCurrentIndex(index);

    QObject::connect(protocole, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProtocolWidget(int)));

    ptcIndex = index;

}




void MainWindow::displayICMPCode(int icmpType)
{
    gridLayout->removeWidget(code);
    //delete code;
    switch(icmpType)
    {
    case 0:
        code->hide();
        break;
    case 3:
        code->show();
        code->clear();
        code->insertItems(0, QStringList()
                          << "0: Destination network unreachable" << "1: Destination host unreachable"
                          << "2: Destination protocol unreachable" << "3: Destination port unreachable"
                          << "4: Fragmentation required, and DF flag set" << "5: Source route failed"
                          << "6: Destination network unknown" << "7: Destination host unknown"
                          << "8: Source host isolated" << "9: Network administratively prohibited"
                          << "10: Host administratively prohibited" << "11: Network unreachable for ToS"
                          << "12: Host unreachable for ToS" << "13: Communication administratively prohibited"
                          << "14: Host Precedence Violation" << "15: Precedence cutoff in effect");
        break;
    case 4:
        code->hide();
        break;
    case 5:
        code->show();
        code->clear();
        code->insertItems(0, QStringList()
                          << "0: Redirect Datagram for the Network" << "1: Redirect Datagram for the Host"
                          << "2: Redirect Datagram for the ToS & network" << "3: Redirect Datagram for the ToS & host");
        break;
    case 8:
        code->hide();
        break;
    }
    gridLayout->addWidget(code, 2, 3, 1, 5);
}

void MainWindow::sendDataEvent()
{
    switch(osiIndex)
    {
    case 0:
        break;
    case 1:
        break;
    case 2:

        switch (ptcIndex) {
        case 0:
            break;
        case 1:
            break;
        case 2:
            {
                unsigned short id = 0x6523; //rand()%65535;

                unsigned char type = 0x00;
                unsigned char codeID = 0x00;

                if(echoReply->isChecked())
                {
                    type = 0x00;
                    codeID = 0x00;
                }
                if(dstUnreach->isChecked())
                {
                    type = 0x03;
                    codeID = code->currentIndex();
                }
                if(srcExtinction->isChecked())
                {
                    type = 0x04;
                    codeID = 0x00;
                }
                if(redirect->isChecked())
                {
                    type = 0x05;
                    codeID = code->currentIndex();
                }
                if(echoRequest->isChecked())
                {
                    type = 0x08;
                    codeID = 0x00;
                }

                IPv4 ipv4_header = {
                    0x45,
                    0x00,
                    0x1c00,
                    id,
                    0x40,//0x20, don't frag; 0x40,
                    0x00,//0xe402, don't frag: 0x00,
                    0x40,
                    IPv4_ICMP
                };

                unsigned char srcIP_v[4];
                unsigned char dstIP_v[4];
                sscanf(sIP->text().toStdString().c_str(), "%hu.%hu.%hu.%hu", srcIP_v, srcIP_v+1, srcIP_v+2, srcIP_v+3);
                sscanf(IP->text().toStdString().c_str(), "%hu.%hu.%hu.%hu", dstIP_v, dstIP_v+1, dstIP_v+2, dstIP_v+3);
                memcpy( ipv4_header.srcIP, srcIP_v, sizeof( srcIP_v ) );
                memcpy( ipv4_header.dstIP, dstIP_v, sizeof( dstIP_v ) );

                unsigned short ipv4_checksum = Util::IPv4Checksum(ipv4_header);

                ipv4_header.checksum = ipv4_checksum;

                ICMP icmp = {
                    ipv4_header,
                    type,
                    codeID,
                    0x0000,
                    0x6565,
                    0x0100,
                };


                unsigned short checksum = 0xFFFF - (type + codeID + 0x6565 + 0x0100);

                memcpy(&icmp.checksum, &checksum, sizeof(checksum));

              //  f0:82:61:ee:9e:44
                unsigned char dest[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

                if(Util::isLocalNetwork(dstIP_v, interfaces->currentText().toStdString()))
                {
                    std::string mac = Util::getMacByIP(IP->text().toStdString(), interfaces->currentText().toStdString());
                    vector<string> macParts = Util::split(mac, ":");

                    for(int i=0; i<6; i++)
                        dest[i] = std::stoi(macParts[i],0,16);

                }
                else
                {
                    std::string cmdGetGWIp = "netstat -nr | grep -e '^0\.0\.0\.0' | awk '{print $2}' | tr -d '\n'";
                    std::string ipGW = Util::exec(cmdGetGWIp.c_str());
                    std::string mac = Util::getMacByIP(ipGW, interfaces->currentText().toStdString());
                    vector<string> macParts = Util::split(mac, ":");

                    for(int i=0; i<6; i++)
                        dest[i] = std::stoi(macParts[i],0,16);
                }

                sendData(icmp, IPv4_protocol, dest, NULL, interfaces->currentText().toStdString().c_str());



            }
            break;
        case 3:

            unsigned short opCode_v = 0x0000;

            unsigned char srcMAC_v[6];
            unsigned char dstMAC_v[6];
            unsigned char srcIP_v[4];
            unsigned char dstIP_v[4];


            if(opCode->currentIndex() == 0)
                opCode_v = 0x0100;
            else if(opCode->currentIndex() == 1)
                opCode_v = 0x0200;


            sscanf(sIP->text().toStdString().c_str(), "%hu.%hu.%hu.%hu", srcIP_v, srcIP_v+1, srcIP_v+2, srcIP_v+3);
            sscanf(IP->text().toStdString().c_str(), "%hu.%hu.%hu.%hu", dstIP_v, dstIP_v+1, dstIP_v+2, dstIP_v+3);
            sscanf(srcMAC->text().toStdString().c_str(), "%02x:%02x:%02x:%02x:%02x:%02x", srcMAC_v, srcMAC_v+1, srcMAC_v+2, srcMAC_v+3, srcMAC_v+4, srcMAC_v+5);
            sscanf(dstMAC->text().toStdString().c_str(), "%02x:%02x:%02x:%02x:%02x:%02x", dstMAC_v, dstMAC_v+1, dstMAC_v+2, dstMAC_v+3, dstMAC_v+4, dstMAC_v+5);



            for (int i=0; i<4; i++)
                qDebug() << srcIP_v[i];
            for (int i=0; i<6; i++)
                qDebug() << srcMAC_v[i];



            ARP arp =
            {
                0x0100,
                0x0008,
                0x06,
                0x04,
                opCode_v,
            };
            memcpy( arp.srcMAC, srcMAC_v, sizeof( srcMAC_v ) );
            memcpy( arp.dstMAC, dstMAC_v, sizeof( dstMAC_v ) );
            memcpy( arp.srcIP, srcIP_v, sizeof( srcIP_v ) );
            memcpy( arp.dstIP, dstIP_v, sizeof( dstIP_v ) );



            unsigned char dest[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
            sendData(arp, ARP_protocol, dest, NULL, interfaces->currentText().toStdString().c_str());

            break;
        }



        break;
    case 3:
        break;
    case 4:
        break;
    case 5:
        break;
    case 6:
        break;

    }
}


