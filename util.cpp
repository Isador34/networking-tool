#include "util.h"

Util::Util()
{

}

string Util::exec(const char* cmd) {
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

vector<std::string> Util::getInterfaces()
{
    std::string cmd = "ls /sys/class/net/";
    std::string interfaces = Util::exec(cmd.c_str());
    return Util::split(interfaces, "\n");
}

vector<string> Util::split(string str, const string sep){
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    std::vector<std::string> arr;
    current=strtok(cstr,sep.c_str());
    while(current!=NULL){
        arr.push_back(current);
        current=strtok(NULL,sep.c_str());
    }
    return arr;
}

unsigned short Util::IPv4Checksum(IPv4 ipv4)
{
    unsigned int sum = 0;

    sum += (ipv4.versionAndHeaderLenght << 8 | ipv4.typeOfService);
    sum += (((ipv4.totalLenght & 0xff)<<8) | ((ipv4.totalLenght & 0xff00)>>8));
    sum += ((ipv4.identification & 0xff)<<8) | ((ipv4.identification & 0xff00)>>8);
    sum += (ipv4.flags << 8 | ipv4.fragOffset);
    sum += (ipv4.ttl << 8 | ipv4.protocol);
    sum += (ipv4.srcIP[0] << 8 | ipv4.srcIP[1]);
    sum += (ipv4.srcIP[2] << 8 | ipv4.srcIP[3]);
    sum += (ipv4.dstIP[0] << 8 | ipv4.dstIP[1]);
    sum += (ipv4.dstIP[2] << 8 | ipv4.dstIP[3]);

    unsigned char first = sum >> 16;
    unsigned short second = (unsigned short) (sum);// & 0xFFFF;

    qDebug() << QString("%1").arg(ipv4.identification, 0, 16);
    qDebug() << QString("%1").arg(sum, 0, 16);
    qDebug() << QString("%1").arg(first, 0, 16);
    qDebug() << QString("%1").arg(second, 0, 16);


    return ((( (0xFFFF - (first + second)) & 0xff)<<8) | (( (0xFFFF - (first + second)) & 0xff00)>>8));
}

std::string Util::getMacByIP(std::string ip, std::string interface)
{
    std::string cmd = "arping -f " + ip + " -I " + interface + " | grep \"reply\" | cut -d ' ' -f 5 | cut -c 2-18";
    qDebug() << cmd.c_str();
    return Util::exec(cmd.c_str());
}

bool Util::isLocalNetwork(unsigned char ip[4], std::string interface)
{
    std::string cmd = "ifconfig "+ interface + " | grep \"inet \"|tr -s ' '|cut -d' ' -f3 | cut -c 5-";
    std::string myIp = Util::exec(cmd.c_str());
    cmd = "ifconfig " + interface + " | grep \"inet \" | tr -s ' ' | cut -d ' ' -f 5 | cut -d ':' -f 2";
    std::string mask = Util::exec(cmd.c_str());

    vector<string> ipParts = Util::split(myIp, ".");
    vector<string> maskParts = Util::split(mask, ".");
    unsigned char network[4] = {0};


    for(int i=0; i<4; i++)
        network[i] = std::stoi(ipParts[i]) & std::stoi(maskParts[i]);

    for(int i=0; i<4; i++)
    {
        if(network[i] != ip[i] & std::stoi(maskParts[i]))
            return false;
    }

    return true;
}
