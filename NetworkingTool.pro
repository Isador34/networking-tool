#-------------------------------------------------
#
# Project created by QtCreator 2016-07-25T14:36:48
#
#-------------------------------------------------

QT       += core gui
QT += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NetworkingTool
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    util.cpp

HEADERS  += mainwindow.h \
    util.h \
    net.h \
    net_struct.h

FORMS    += mainwindow.ui
