#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <stdint.h>
#include <QDebug>

#include "net_struct.h"

using namespace std;

class Util
{
public:
    Util();
    static string exec(const char* cmd);
    static vector<std::string> getInterfaces();
    static bool isLocalNetwork(unsigned char ip[4], std::string interface);
    static std::string getMacByIP(std::string ip, std::string interface);
    static unsigned short IPv4Checksum(IPv4 ipv4);
    static vector<string> split(string str, string sep);
};

#endif // UTIL_H
