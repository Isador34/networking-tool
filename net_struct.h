#ifndef NET_STRUCT_H
#define NET_STRUCT_H

static const unsigned short ARP_protocol = 0x0806;
static const unsigned short IPv4_protocol = 0x0800;
static const unsigned short CDP_protocol = 0x2000;

static const unsigned char IPv4_ICMP = 0x01;
static const unsigned char IPv4_UDP = 0x11;
static const unsigned char IPv4_TCP = 0x06;

typedef struct{ // LITTLE ENDIAN
    unsigned short hdwType;
    unsigned short ptcType;
    unsigned char hdwSize;
    unsigned char ptcSize;
    unsigned short opCode;

    unsigned char srcMAC[6];
    unsigned char srcIP[4];
    unsigned char dstMAC[6];
    unsigned char dstIP[4];
} ARP;

typedef struct{
    unsigned char versionAndHeaderLenght;
    unsigned char typeOfService;
    unsigned short totalLenght;
    unsigned short identification;
    unsigned char flags;
    unsigned char fragOffset;
    unsigned char ttl;
    unsigned char protocol;
    unsigned short checksum; //versionAndHeaderLenght + typeOfService + totalLenght + identification + flags + fragOffset + ttl + protocol
    unsigned char srcIP[4];
    unsigned char dstIP[4];
} IPv4;

typedef struct{
    IPv4 ipv4;
    unsigned char type;
    unsigned char code;
    unsigned short checksum;
    unsigned short identifier;
    unsigned short seqNumb;
} ICMP;

typedef struct{
    IPv4 ipv4;
    unsigned short sPort;
    unsigned short dPort;
    unsigned int seqNumber;
    unsigned int ackNumber;
    unsigned char headerLenght;
    unsigned char flags;
    unsigned short window;
    unsigned short checksum;
    unsigned short urgentPointer;
    unsigned char options[12];
}TCP;

typedef struct{
    IPv4 ipv4;
    unsigned short sPort;
    unsigned short dPort;
    unsigned short lenght;
    unsigned short checksum;
}UDP;


#endif // NET_STRUCT_H
