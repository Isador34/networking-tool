#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>    /* Must precede if*.h */
#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <unistd.h>

static const unsigned short ARP_protocol = 0x0806;
static const unsigned short IPv4_protocol = 0x0800;



typedef struct{ // LITTLE ENDIAN
	unsigned short hdwType;
	unsigned short ptcType;
	unsigned char hdwSize;
	unsigned char ptcSize;
	unsigned short opCode;
  
	unsigned char srcMAC[6];
	unsigned char srcIP[4];
	unsigned char dstMAC[6];
	unsigned char dstIP[4];
} ARP;

typedef struct{
	unsigned char versionAndHeaderLenght;
	unsigned char typeOfService;
	unsigned short totalLenght;
	unsigned short identification;
	unsigned char flags;
	unsigned char fragOffset;
	unsigned char ttl;
	unsigned char protocol;
	unsigned short checksum; //versionAndHeaderLenght + typeOfService + totalLenght + identification + flags + fragOffset + ttl + protocol
	unsigned char srcIP[4];
	unsigned char dstIP[4];
} IPv4;


union ethframe
{
  struct
  {
    struct ethhdr    header;
    unsigned char    data[ETH_DATA_LEN];
  } field;
  unsigned char    buffer[ETH_FRAME_LEN];
};


template< typename T >
void sendData( T const & param , unsigned short proto) {
  char *iface = "wlan0";
  unsigned char dest[ETH_ALEN]
           = { 0x62, 0xF1, 0x89, 0x75, 0x36, 0xEA }; // 62:F1:89:75:36:EA
 
  int s;
  if ((s = socket(AF_PACKET, SOCK_RAW, htons(proto))) < 0) {
    printf("Error: could not open socket\n");
    
  }
 
  struct ifreq buffer;
  int ifindex;
  memset(&buffer, 0x00, sizeof(buffer));
  strncpy(buffer.ifr_name, iface, IFNAMSIZ);
  if (ioctl(s, SIOCGIFINDEX, &buffer) < 0) {
    printf("Error: could not get interface index\n");
    close(s);
    
  }
  ifindex = buffer.ifr_ifindex;
 
  unsigned char source[ETH_ALEN];
  if (ioctl(s, SIOCGIFHWADDR, &buffer) < 0) {
    printf("Error: could not get interface address\n");
    close(s);
    
  }
  memcpy((void*)source, (void*)(buffer.ifr_hwaddr.sa_data),
         ETH_ALEN);
 
  union ethframe frame;
  memcpy(frame.field.header.h_dest, dest, ETH_ALEN);
  memcpy(frame.field.header.h_source, source, ETH_ALEN);
  frame.field.header.h_proto = htons(proto);
  
  memcpy(frame.field.data, &param, sizeof(param));
 
  unsigned int frame_len = ETH_HLEN + sizeof(param);
 
  struct sockaddr_ll saddrll;
  memset((void*)&saddrll, 0, sizeof(saddrll));
  saddrll.sll_family = PF_PACKET;   
  saddrll.sll_ifindex = ifindex;
  saddrll.sll_halen = ETH_ALEN;
  memcpy((void*)(saddrll.sll_addr), (void*)dest, ETH_ALEN);
 
  if (sendto(s, frame.buffer, frame_len, 0, (struct sockaddr*)&saddrll, sizeof(saddrll)) > 0)
	printf("Success!\n");
  else
    printf("Error, could not send\n");
 
  close(s);
 
}

int main()
{
	ARP arp = 
	{
		0x0100,
		0x0008,
		0x06,
		0x04,
		0x0100,
		
		{0x58, 0x91, 0xcf, 0x3d, 0xac, 0xfe},
		{0xc0, 0xa8, 0x4b, 0x54},
		{0xff, 0xff, 0xff, 0xff, 0xff, 0xef},
		{0xc0, 0xa8, 0x2b, 0x81},
	};
	
	sendData(arp, ARP_protocol);
	
	return 0;
}

